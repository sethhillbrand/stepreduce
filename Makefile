stepreduce: stepreduce.cpp
	c++ -O3 -std=c++11 -lstdc++ -o $@ $^

debug: stepreduce.cpp
	c++ -O0 -std=c++11 -ggdb -lstdc++ -o $@ $^

.PHONY: clean all default
all: stepreduce
default: all
clean:
	rm -f stepreduce
